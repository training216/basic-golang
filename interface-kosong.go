package main

import "fmt"

// interface kosong, interface yg tdk memiliki deklarasi method satupun
// membuat otomatis semua tipe data
func Ups(i int, apapun interface{}) interface{} {
	if i == 1 {
		return 1
	} else if i == 2 {
		return  true
	} else {
		return "Ups"
	}
}

func main() {
	var data interface{} = Ups(1, true)
	fmt.Println(data)
}
