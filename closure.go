package main

import "fmt"

func main() {
	name := "Alam"
	counter := 0

	// closures mrpkn kemampuan sebuah function berinteraks dng data" di sekitar
	increment := func() {
		name := "Rachmat"
		fmt.Println("Increment")
		counter++
		// output rachmat
		fmt.Println(name)
	}

	increment()
	increment()
	fmt.Println(counter)
	// outuput alam
	fmt.Println(name)

}
