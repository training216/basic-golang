package main

import "fmt"

func factorialLoop(value int)  int {
	result := 1
	for i := value; i > 0; i-- {
		result *= i
		//fmt.Println(result)
		//fmt.Println(i)
		//fmt.Println("======")

		//5
		// 4 * 5 20
		// 3 * 20 60
		// 2 * 60 120
		// 1 * 120 120
	}

	return result
}

// recrusive function adalah function yg memanggil function dirinya sendiri
// contoh faktorial
func factorialRecrusive(value int) int  {
	if value == 1 {
		return 1
	} else {
		fmt.Println(value)
		//return factorialRecrusive(value - 1)
		return value * factorialRecrusive(value - 1)
	}
}

func main() {
	loop := factorialLoop(5)
	recursive := factorialRecrusive(5)
	fmt.Println(loop)
	fmt.Println(recursive)
}
