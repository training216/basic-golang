package main

import "fmt"

func getFullName() (string, string, string) {
	return "Alam", "Rachmat", "Kurniawan"
}

func main() {
	firstName, _, lastName := getFullName()
	fmt.Println(firstName)
	fmt.Println(lastName)
}
