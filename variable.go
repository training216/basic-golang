package main

import "fmt"

func main() {
	var name string

	name = "Alam Rachmat"
	fmt.Println(name)

	name = "Alam Kurniawan"
	fmt.Println(name)

	var friendName = "Eko"
	fmt.Println(friendName)

	country := "Indonesia"
	fmt.Println(country)

	// multiple variable
	var (
		firstName = "Alam"
		lastName = "Kurniawan"
	)

	fmt.Println(firstName)
	fmt.Println(lastName)
}
