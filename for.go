package main

import "fmt"

func main() {
	//di golang hanya ada 1 perulangan yaitu for. while & do while tidak ada
	counter := 1

	for counter < 10 {
		fmt.Println("Perulangan ke : ", counter)
		counter++
	}

	// counter2 := 1 init statement, counter2++ post statement
	for counter2 := 1; counter2 < 10; counter2++ {
		fmt.Println(" Looping ke : ", counter2)
	}

	slice := []string{"Alam", "Rachmat", "Kurniawan"}

	for i := 0; i < len(slice); i++ {
		fmt.Println(slice[i])
	}

	// for range
	// key, value
	for index, value := range slice {
		fmt.Println("Perulangan ke ", index, " : ", value)
	}

	person := make(map[string]string)
	person["name"] = "Maryam"
	person["status"] = "Anak"

	for key, value := range(person) {
		fmt.Println("Perulangan ", key, " : ", value)
	}

}
