package main

import "fmt"

// interface untuk membuat kontrak
// contoh strucr berbeda, func sama

type HasName interface {
	// yg mempunya function GetName dan return value string maka dia berhak mrngikuti kontrak sayHello
	GetName(param string) string // boleh menambahkan struct function
}

func SayHello(hasName HasName)  {
	fmt.Println("Hello", hasName.GetName("Parameter"))
}

// person belum punya kontrak GetName
type Person struct {
	Name string
}

// person sudah punya kontrak GetName
func (person Person) GetName(param string) string {
	return person.Name + param
}

type Animal struct {
	Name string
}

func (animal Animal) GetName(param string) string {
	return animal.Name + " " + param
}

func main() {
	var eko Person
	eko.Name = "Eko"
	// function harus punya kontrak GetName
	SayHello(eko)

	animal := Animal{
		Name: "Push",
	}
	SayHello(animal)
}
