package main

import "fmt"

func main() {
	var name = "Alam"
	// switch bentuk sederhana if
	switch name {
	case "Alam":
		fmt.Println("Hello Alam")
	case "Rachmat":
		fmt.Println("Hello Rachmat")
	}

	// switch short statement
	switch length:= len(name); length > 5{
	case true:
		fmt.Println("Lebih dari 5")
	case false:
		fmt.Println("Kurang dari 5")
	}

	// percabangan mirip if, kondisi percabangan sederhana
	length2 := len(name)
	switch  {
	case length2 > 10:
		fmt.Println("Nama terlalu panjang")
	case length2 > 5:
		fmt.Println("Nama lumayan panjang")
	default:
		fmt.Println("Nama sudah benar")
	}
}
