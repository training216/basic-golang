package main

import "fmt"

func main() {

	// berbeda dengan array dan slice, index pada map dapat di tentukan
	person := map[string]string{
		"name": "Alam",
		"address": "Jakarta",
	}

	person["title"] = "Programmer"

	fmt.Println(person)
	fmt.Println(person["name"])
	fmt.Println(person["address"])

	// var book  = make(map[string]string)
	var book map[string]string = make(map[string]string)
	book["titile"] = "Belajar Golang"
	book["author"] = "Alam"
	book["ups"] = "Salah"

	delete(book, "ups")

	fmt.Println(book)
	fmt.Println(len(book))
}
