package main

import "fmt"

func main() {
	type NoKtp string
	type Married bool

	var noKtpAlam NoKtp = "123456778"
	var marriedStatus Married = true

	fmt.Println(noKtpAlam)
	fmt.Println(marriedStatus)
}
