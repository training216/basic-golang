package main

import "fmt"

func main() {
	/*
	const tanpa println bisa di runing sedangkan variable tidak
	 */
	const firstName string = "Alam"
	const lastName = "Rachmat"
	const value = 1000

	// multiple const
	const (
		fName = "Alam"
		lName = "Rachmat"
	)

	fmt.Println(fName)
	fmt.Println(lName)
}
