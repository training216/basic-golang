package main

import "fmt"

func endApp()  {
	// default nil
	// recover di taro di defer function
	message := recover()
	fmt.Println("Error dengan message : ", message)
	fmt.Println("Aplikasi Selesai")
}

func runApp(error bool) {
	defer endApp()
	if error {
		// panic function adalah function yg bs kita gunakan utk menghentikan program
		panic("APLIKASI ERROR")
	}
}

func main()  {
	runApp(true)
	// jika recover, maka akan melanjuti ke step berikutnya
	fmt.Println("Alam")
}

