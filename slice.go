package main

import "fmt"

func main() {

	// slice : pointer, length, capacity
	var months = [...]string{
		"Januari",
		"Februari",
		"Maret",
		"April",
		"Mei",
		"Juni",
		"Juli",
		"Agustus",
		"September",
		"Oktober",
		"November",
		"Desember",
	}

	// membuat slice di array dimulai dari index low dan sampai index sebelum high
	var slice1 = months[4:7]
	fmt.Println(slice1)
	fmt.Println(len(slice1))
	fmt.Println(cap(slice1))

	months[5] = "Judi Edit"

	fmt.Println(slice1)

	// membuat slice di array dimulai dari index low dan sampai index akhir di array
	// var slice2 = months[11:]

	// membuat slice di array dimulai dari index low dan sampai index sebelum high
	var slice2 = months[2:4]
	fmt.Println("test 1 : ", slice2)

	// append hanya bisa menggunakan slice
	var slice3 = append(slice2, "Alam")
	fmt.Println("test 2 : ",slice3)
	slice3[1] = "Bukan-Desember"
	slice3[2] = "Bukan-Desember2"
	fmt.Println("test 3 : ",slice3)

	fmt.Println("test 4 : ",slice2)
	fmt.Println("test 5 : ",months)

	// var newSlice = make([]string, 2, 5)
	newSlice := make([]string, 2, 5)
	newSlice[0] = "Alam"
	newSlice[1] = "Rachmat"

	fmt.Println(newSlice)

	copySlice := make([]string, len(newSlice), cap(newSlice))
	copy(copySlice, newSlice)
	fmt.Println(copySlice)

}
