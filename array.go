package main

import "fmt"

func main() {
	var names [3]string

	names[0] = "Alam"
	names[1] = "Rachmat"
	names[2] = "Kurniawan"

	fmt.Println(names[0])
	fmt.Println(names[1])
	fmt.Println(names[2])

	var values = [3]int{
		90,
		95,
		80,
	}

	fmt.Println(values)

	var values2 = [3]int{}
	values2[0] = 10
	values2[1] = 20
	fmt.Println(values2[0])

	fmt.Println(len(names))
	fmt.Println(len(values))

}
