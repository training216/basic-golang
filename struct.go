package main

import "fmt"

// struct adalah kumpulan field
// template data, membuat object
type CustomerPremium struct {
	Name, Address string
	Age int
}

// struct function
func (customer CustomerPremium) sayHi(name string)  {
	fmt.Println("Hello", name, "My Name is", customer.Name)
}

func main() {
		var data CustomerPremium
		data.Name = "Alam"
		data.Address = "Indonesia"
		data.Age = 30

		fmt.Println(data)

		// var joko CustomerPremium - CustomerPremium{}
		joko := CustomerPremium{
			Name:    "Joko",
			Address: "Cirebon",
			Age:     35,
		}

		fmt.Println(joko)

		budi := CustomerPremium{"Budi", "Jakarta", 5}
		fmt.Println(budi)

		joko.sayHi("Alam")
}
