package main

import "fmt"

// akan return (firstName, middleName, lastName)
func getFullName2() (firstName string, middleName string, lastName string)  {
	firstName = "Alam"
	middleName = "Rachmat"
	lastName = "Kurniawan"

	return
}

func main() {
	a, b, c := getFullName2()
	fmt.Println(a)
	fmt.Println(b)
	fmt.Println(c)
}
