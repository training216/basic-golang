package main

import "fmt"

func logging() {
	fmt.Println("Selesai memanggil function")
}

func runApplication(value int)  {
	// defer function adalah function yg di jadwalkan di eksekusi setelah sebuah function selesai di eksekusi
	defer logging()
	fmt.Println("Run Application")
	result := 10 / value
	fmt.Println("Result : ", result)
}

func main() {
	runApplication(0)
}
