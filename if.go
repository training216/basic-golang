package main

import "fmt"

func main()  {
	var name = "Alam2"

	if name == "Alam" {
		fmt.Println("Hello : ", name)
	} else if name == "Joko" {
		fmt.Println("Hello Budi")
	}else {
		fmt.Println("Hello, boleh kenalan ga?")
	}

	// if short statement
	if length := len(name); length > 5 {
		fmt.Println("")
	} else {
		fmt.Println("Nama sudah benar")
	}

}